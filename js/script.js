const submitButton = document.getElementById('submit-button');
const overlay = document.getElementById('overlay');
const closeButton = document.getElementById('close-button');

submitButton.addEventListener('click', () => {
    overlay.style.display = 'block';
});

closeButton.addEventListener('click', () => {
    overlay.style.display = 'none';
});
